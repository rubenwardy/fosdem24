## Licenses

* Minetest
  * Original CC BY-SA 3.0 to erlehmann
  * Sticker version made by Lemente
  * Intended to be printed at 4x4cm, but was printed at 3x3 inches
* Lua
  * By Alexandre Nakonechnyj. See https://www.lua.org/images/
  * Printed at 4x4cm
