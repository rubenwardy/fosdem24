# Minetest in Education leaflet

Created by Lemente and Thomate. January 2024.

License: CC-BY-SA 4.0

* Pixel art map is CC0
* Minetest Edu logo is based on the Minetest logo by erlehmann, CC-BY-SA 3.0
* UBC Minetest Classroom illustration by Paul D Pickell, CC-BY-SA 4.0
* Minestory illustration by Julien Crémoux and photo by Olivier Anicet
* UNEJ photo by Giacomo Gilmozzi
* Maubeuge illustration and photo by Thomate and Lemente, CC-BY-SA 4.0
* rpg16 textures by Hugues Ross, CC-BY-SA-4.0

The source of the leaflet is currently unavailable.

